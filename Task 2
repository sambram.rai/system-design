Refer the concept sketch

stages:
  - build
  - test
  - provision
  - configure
  - deploy
  - monitor

variables:
  TF_VAR_aws_access_key: "${AWS_ACCESS_KEY_ID}"
  TF_VAR_aws_secret_key: "${AWS_SECRET_ACCESS_KEY}"

build:
  stage: build
  image: node:14
  script:
    - npm install
    - ng build --prod
  artifacts:
    paths:
      - dist/

unit_test:
  stage: test
  image: node:14
  script:
    - npm install
    - npm test
    - pip install -r requirements.txt
    - pytest

integration_test:
  stage: test
  image: python:3.8
  script:
    - pip install -r requirements.txt
    - pytest integration_tests/

e2e_test:
  stage: test
  image: cypress/base:10
  script:
    - npm install
    - npm run e2e

provision:
  stage: provision
  image: hashicorp/terraform:light
  script:
    - cd terraform
    - terraform init
    - terraform apply -auto-approve

configure:
  stage: configure
  image: williamyeh/ansible:alpine3
  script:
    - ansible-playbook -i inventory playbook.yml

deploy:
  stage: deploy
  image: amazon/aws-cli
  script:
    - aws s3 sync dist/ s3://your-bucket-name
    - kubectl apply -f deployment.yml

monitor:
  stage: monitor
  script:
    - cloudwatch setup scripts